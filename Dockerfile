FROM imbios/bun-node:20-slim

WORKDIR /app
COPY ./bun.lockb .
COPY ./package.json .

RUN bun install

COPY ./src ./src
COPY ./gulpfile.coffee .

RUN bun run build
