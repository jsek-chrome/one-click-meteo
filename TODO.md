# Roadmap

## Options

- Switch: UM / COAMPS (60h / 84h forecast)
- Selecting coordinates from the map (component from m.meteo)
- Allow "named" coordinates - saving my own location
- Favorite cities
- Descriptions (avoid confusing form like in deprecated example)
  - Inform about mobile-friendly page (http://m.meteo.pl/)
- Switch pl/en

## Popup

- Toggle legend
- Discreet link/icon to options
- Switch between favorite cities
- Allow switching view to previous forecasts (subtle pagination with dots..?)
- Toggle text expert comment
- Link to interactive view `beta.meteo.pl/meteogramy?lat={lat}&lng={lng}`

## Performance

- Display previous
  - display loader if forecast is older than 4h
  - offline? just display subtle warning
  - older than 1d? avoid displaying unless loading failed and user clicks "last loaded forecast"

## Offline support

- Preload after start and continuously, but only if prev. is older than min. 4h

## Infrastructure

- Analytics
- Monitoring
- Uninstall page