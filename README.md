# [One-Click Meteo](https://chrome.google.com/webstore/detail/fkakahlhimilfhdkjflfbkgikihondng)

Google Chrome extension that offers quick access to current weather from Meteo.pl.

---

![screenshot](images/screenshot.png)

---

Setup:

```sh
bun i
bunx crx keygen
```

Build:

```sh
bun run build
```

Pack:

```sh
mkdir bin
7z a bin/extension.zip ./dist/*
```
