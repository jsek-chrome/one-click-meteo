gulp = require 'gulp'
$ = require('gulp-load-plugins')()
fs = require 'fs'
join = require('path').join
ChromeExtension = require 'crx'

series = (x) -> gulp.series.apply gulp, x
parallel = (x) -> gulp.parallel.apply gulp, x


# #####################################
# Compilation
#

gulp.task 'build:coffee', ->
    gulp.src 'src/**/*.coffee'
        .pipe $.plumber()
        .pipe $.coffee()
        .pipe gulp.dest 'dist'

gulp.task 'build:sass', ->
	gulp.src 'src/**/*.scss'
        .pipe $.plumber()
		.pipe $.postcss([
			require('precss')(import: extension: 'scss')
		])
		.pipe $.rename (x) -> x.extname = '.css'
        .pipe gulp.dest 'dist'

gulp.task 'build:pug', ->
    gulp.src 'src/**/*.pug'
        .pipe $.plumber()
        .pipe $.pug()
        .pipe gulp.dest 'dist'


# #####################################
# Copy libraries and static assets
#

gulp.task 'copy:manifest', ->
    gulp.src 'src/manifest.json'
        .pipe gulp.dest 'dist'

gulp.task 'copy:fonts', ->
    gulp.src [
            'node_modules/materialize-css/dist/fonts/**/*.*'
            'node_modules/mdi/fonts/**/*.*'
        ]
        .pipe gulp.dest 'dist/fonts'

gulp.task 'copy:images', ->
    gulp.src 'src/**/*.+(png|jpg|gif|svg)'
        .pipe gulp.dest 'dist'

gulp.task 'copy:libs', ->
    gulp.src [
            'node_modules/jquery/dist/jquery.min.js'
            'node_modules/materialize-css/dist/js/materialize.min.js'
            'node_modules/materialize-css/dist/css/materialize.min.css'
        ]
        .pipe gulp.dest 'dist/libs'


# #####################################
# Build everything
#

gulp.task 'build', parallel [
    'copy:libs'
    'copy:fonts'
    'copy:images'
    'copy:manifest'
    'build:coffee'
    'build:sass'
    'build:pug'
]


# #####################################
# Clean everything
#

gulp.task 'clean', ->
    require('del') ['dist', 'bin']


# #####################################
# Create packages
#

gulp.task 'pack:zip', () ->
    {'7z' : _7z} = require('7zip')
    {spawn} = require('child_process')
    spawn(_7z, ['a', 'bin/extension.zip', './dist/*'])
    # 7z a bin/extension.zip ./dist/*

gulp.task 'pack:crx', (done) ->
    crx = new ChromeExtension
        privateKey: fs.readFileSync(join(__dirname, "key.pem"))
    crx.load(join(__dirname,'dist'))
        .then ->
            crx.pack()
                .then (crxBuffer) ->
                    fs.writeFile join(__dirname,'bin','extension.crx'), crxBuffer, (err) ->
                        console.log err if err
                        done()

gulp.task 'pack', parallel [
    'pack:crx'
    'pack:zip'
]

gulp.task 'default', series [
    'clean'
    'build'
    'pack'
]


# #####################################
# Watch for changes (dev mode)
#

gulp.task 'watch', ->
    gulp.watch 'src/**/*.coffee',   parallel ['build:coffee']
    gulp.watch 'src/**/*.scss',     parallel ['build:sass']
    gulp.watch 'src/**/*.pug',      parallel ['build:pug']
    gulp.watch 'src/manifest.json', parallel ['copy:manifest']
